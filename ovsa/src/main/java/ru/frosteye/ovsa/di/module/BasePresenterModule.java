package ru.frosteye.ovsa.di.module;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by ovcst on 07.01.2017.
 */

public abstract class BasePresenterModule<A extends AppCompatActivity, F extends Fragment> {

    private A activity;
    private View view;
    private F fragment;

    public BasePresenterModule(View view) {
        this.view = view;
    }

    public BasePresenterModule(A activity) {
        this.activity = activity;
    }

    public BasePresenterModule(F fragment) {
        this.fragment = fragment;
    }

    public BasePresenterModule() {

    }

    protected Context getContext() {
        if(activity != null) return activity;
        if(fragment != null) return fragment.getContext();
        if(view != null) return view.getContext();
        throw new IllegalStateException("Presenter module is not initialized");
    }
}
