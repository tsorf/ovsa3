package ru.frosteye.ovsa.presentation.view.activity

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.os.Parcelable
import android.support.annotation.ArrayRes
import android.support.annotation.ColorRes
import android.support.graphics.drawable.AnimatedVectorDrawableCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView

import java.io.Serializable
import java.util.HashMap

import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.presentation.callback.SimpleTextChangeCallback
import ru.frosteye.ovsa.stub.impl.SimpleTextWatcher
import ru.frosteye.ovsa.stub.listener.OnDoneListener
import ru.frosteye.ovsa.stub.listener.SelectListener
import ru.frosteye.ovsa.tool.TextTools
import ru.frosteye.ovsa.tool.UITools
import ru.frosteye.ovsa.tool.isTrimmedEmpty

/**
 * Created by oleg on 21.06.16.
 */
abstract class OvsaActivity : AppCompatActivity() {
    private val textChangeListeners = HashMap<SimpleTextWatcher, (Editable) -> Unit>()
    private var backButtonEnabled = false
    private var topLoadingShown = false

    val loaderDrawable: AnimatedVectorDrawableCompat?
        get() {
            try {

                return AnimatedVectorDrawableCompat.create(this, R.drawable.ovsa_menu_loader_animated)
            } catch (e: Exception) {
                return null
            }

        }


    protected fun registerTextChangeListeners(changeCallback: (Editable) -> Unit, vararg views: TextView) {
        val textWatcher = object : SimpleTextWatcher() {
            override fun afterTextChanged(editable: Editable) {
                textChangeListeners[this]?.invoke(editable)
            }
        }
        for (view in views) {
            view.addTextChangedListener(textWatcher)
        }
        textChangeListeners[textWatcher] = changeCallback
    }

    protected fun registerTextChangeListeners(viewToEnable: View, vararg views: TextView) {
        val textWatcher = object : SimpleTextWatcher() {
            override fun afterTextChanged(editable: Editable) {

                viewToEnable.isEnabled = checkEnabledConditions(viewToEnable, *views)
            }
        }
        for (view in views) {
            view.addTextChangedListener(textWatcher)
        }
    }

    protected fun applyBackArrowColor(@ColorRes color: Int) {
        try {
            val upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material)
            upArrow!!.setColorFilter(ContextCompat.getColor(this, color), PorterDuff.Mode.SRC_ATOP)
            supportActionBar!!.setHomeAsUpIndicator(upArrow)

        } catch (ignored: Exception) {
        }

    }

    fun startActivityAndClearTask(clazz: Class<out Activity>) {
        val intents = Intent(this, clazz)
        startActivityAndClearTask(intents)
    }

    fun startActivityAndClearTask(intents: Intent) {
        intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                or Intent.FLAG_ACTIVITY_CLEAR_TOP
                or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intents)
        finish()
    }

    protected fun checkEnabledConditions(viewToEnable: View, vararg views: TextView): Boolean {
        for (view in views) {
            if (view.isTrimmedEmpty()) {
                return false
            }
        }
        return true
    }

    fun setEditTextColorFilter(input: EditText, color: Int) {
        input.background.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }

    protected open fun enableBackButton() {
        try {
            supportActionBar?.let {
                it.setDisplayHomeAsUpEnabled(true)
                backButtonEnabled = true
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    protected fun <T> getSerializable(key: String, clazz: Class<T>): T? {
        if (intent.extras == null) return null
        val serializable = intent.getSerializableExtra(key)
        return if (serializable != null) {
            try {
                clazz.cast(serializable)
            } catch (e: Exception) {
                null
            }

        } else
            null
    }

    protected fun setOnDoneListener(textView: TextView, onDoneListener: () -> Unit) {
        textView.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                onDoneListener.invoke()
            }
            false
        }
    }

    open fun showTopBarLoading(shown: Boolean) {
        this.topLoadingShown = shown
        invalidateOptionsMenu()
    }

    protected fun openUrl(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        try {
            val icon = AnimatedVectorDrawableCompat.create(this, R.drawable.ovsa_menu_loader_animated)
            menu.add(0, MENU_LOADING, 10, "Loading")
                    .setIcon(icon)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        } catch (ignored: Exception) {
        }

        return super.onCreateOptionsMenu(menu)
    }

    fun startActivity(clazz: Class<out Activity>) {
        val intent = Intent(this, clazz)
        startActivity(intent)
    }

    fun startActivity(clazz: Class<out Activity>, vararg payloads: Parcelable?) {
        val intent = Intent(this, clazz)
        payloads.forEachIndexed { i, p ->
            p?.let {
                val name = p::class.java.name
                intent.putExtra(name, p)
            }
        }
        startActivity(intent)
    }

    fun startActivityForResult(clazz: Class<out Activity>, code: Int, vararg payloads: Parcelable?) {
        val intent = Intent(this, clazz)
        payloads.forEachIndexed { i, p ->
            p?.let {
                val name = p::class.java.name
                intent.putExtra(name, p)
            }
        }
        startActivityForResult(intent, code)
    }

    fun startActivity(clazz: Class<out Activity>, items: Map<String, Parcelable>) {
        val intent = Intent(this, clazz)
        items.forEach {
            intent.putExtra(it.key, it.value)

        }
        startActivity(intent)
    }

    fun startActivity(clazz: Class<out Activity>, finish: Boolean) {
        val intent = Intent(this, clazz)
        startActivity(intent)
        if (finish) finish()
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        processTopLoading(menu)
        return super.onPrepareOptionsMenu(menu)
    }

    private fun processTopLoading(menu: Menu) {
        try {
            val menuItemLoader = menu.findItem(MENU_LOADING)
            val menuItemLoaderIcon = menuItemLoader.icon
            if (menuItemLoaderIcon != null) {
                if (topLoadingShown) {
                    menuItemLoader.isVisible = true
                    (menuItemLoader.icon as Animatable).start()
                } else {
                    menuItemLoader.isVisible = false
                }
            }
        } catch (ignored: Exception) {

        }

    }

    protected fun showKeyboard(editText: EditText) {
        Handler().postDelayed({
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }, 500)
    }


    @Deprecated("")
    fun showMessage(message: String) {
        UITools.toastLong(this, message)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (!backButtonEnabled) return true
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    @JvmOverloads
    protected fun showSelect(context: Context,
                             @ArrayRes res: Int,
                             listener: SelectListener, notCancelable: Boolean = false) {
        val titles = context.resources.getStringArray(res)
        showSelect(context, titles, listener, notCancelable)
    }

    protected fun showSelect(context: Context,
                             items: Array<String>,
                             listener: SelectListener, notCancelable: Boolean) {
        val builder = AlertDialog.Builder(context)
        builder.setItems(items) { dialogInterface, i -> listener.onSelect(items[i], i) }
        builder.setCancelable(!notCancelable)
        builder.show()
    }

    fun showError(error: String) {
        showToast(error)
    }

    fun showToast(message: String) {
        UITools.toastLong(this, message)
    }

    fun showSuccess(message: String) {
        showToast(message)
    }


    fun showMessage(message: CharSequence?, code: Int) {
        when (code) {
            -1 -> showError(message.toString())
            0 -> showToast(message.toString())
            else -> showSuccess(message.toString())
        }
    }

    companion object {
        val TAG = OvsaActivity::class.java.simpleName
        private val MENU_LOADING = 123
    }
}
