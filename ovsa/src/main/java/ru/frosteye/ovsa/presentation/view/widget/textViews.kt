package ru.frosteye.ovsa.presentation.view.widget

import android.content.Context
import android.graphics.Paint
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.Scroller
import android.widget.TextView

/**
 * Created by ovcst on 30.05.2018.
 */
/**
 * A TextView that scrolls it contents across the screen, in a similar fashion as movie credits roll
 * across the theater screen.
 *
 * @author Matthias Kaeppler
 */
class ScrollingTextView : TextView, Runnable {

    var customScroller: Scroller? = null
    var speed = DEFAULT_SPEED
    var isContinuousScrolling = true

    constructor(context: Context) : super(context) {
        setup(context)
    }

    constructor(context: Context, attributes: AttributeSet) : super(context, attributes) {
        setup(context)
    }

    private fun setup(context: Context) {
        customScroller = Scroller(context, LinearInterpolator())
        setScroller(customScroller)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (customScroller!!.isFinished) {
            scroll()
        }
    }

    private fun scroll() {
        val viewHeight = height
        val visibleHeight = viewHeight - paddingBottom - paddingTop
        val lineHeight = lineHeight

        val offset = -1 * visibleHeight
        val distance = visibleHeight + lineCount * lineHeight
        val duration = (distance * speed).toInt()

        customScroller!!.startScroll(0, offset, 0, distance, duration)

        if (isContinuousScrolling) {
            post(this)
        }
    }

    override fun run() {
        if (customScroller!!.isFinished) {
            scroll()
        } else {
            post(this)
        }
    }

    companion object {

        private val DEFAULT_SPEED = 15.0f
    }
}

/**
 * Created by ovcst on 16.05.2017.
 */

class BlinkingTextView : AppCompatTextView {

    private var anim: Animation? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onFinishInflate() {
        super.onFinishInflate()
        start()
    }

    fun start() {
        stop()
        anim = AlphaAnimation(0.4f, 1.0f)
        anim!!.duration = 1000
        anim!!.startOffset = 20
        anim!!.repeatMode = Animation.REVERSE
        anim!!.repeatCount = Animation.INFINITE
        startAnimation(anim)
    }

    fun stop() {
        if (anim != null) {
            anim!!.cancel()
            anim = null
        }
        alpha = 1.0f
    }

    override fun setVisibility(visibility: Int) {
        if (visibility == View.GONE) {
            clearAnimation()
        } else {
            startAnimation(anim)
        }
        super.setVisibility(visibility)
    }
}

/**
 * Created by oleg on 05.12.16.
 */

class UnderlinedTextView : AppCompatTextView {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }
}