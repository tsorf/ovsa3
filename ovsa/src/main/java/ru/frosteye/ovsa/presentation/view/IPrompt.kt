package ru.frosteye.ovsa.presentation.view

/**
 * Created by oleg on 25.09.17.
 */

interface IPrompt {
    fun title(): String
    fun hint(): String
    fun positiveButton(): String
    fun negativeButton(): String
    fun text(): String
    fun inputType(): Int
    fun cancelable(): Boolean

    interface Listener {
        fun onResult(text: String)
        fun onCancel()
    }
}
