package ru.frosteye.ovsa.presentation.view.widget

import android.content.Context
import android.content.res.TypedArray
import android.os.Build
import android.support.annotation.LayoutRes
import android.support.annotation.RequiresApi
import android.support.annotation.StyleableRes
import android.support.constraint.ConstraintLayout
import android.support.v4.view.ViewCompat
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import ru.frosteye.ovsa.tool.UITools

/**
 * Created by ovcst on 30.05.2018.
 */
/**
 * Created by oleg on 23.12.16.
 */

fun View.getStyleable(context: Context) : IntArray? {
    return try {
        UITools.getResourceDeclareStyleableIntArray(context, javaClass.simpleName)
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

abstract class BaseConstraintLayout : ConstraintLayout {


    open val layoutToInflate: Int = 0

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

/**
 * Created by oleg on 23.12.16.
 */

abstract class BaseFrameLayout : FrameLayout {

    open protected val layoutToInflate: Int
        @LayoutRes
        get() = 0

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, defStyleRes))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

/**
 * Created by oleg on 23.12.16.
 */

abstract class BaseImageView : AppCompatImageView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareView()
    }

    open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

/**
 * Created by oleg on 23.12.16.
 */

abstract class BaseLinearLayout : LinearLayout {

    open val layoutToInflate: Int = 0

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, defStyleRes))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}

/**
 * Created by oleg on 23.12.16.
 */

abstract class BaseRelativeLayout : RelativeLayout {

    protected open val layoutToInflate: Int
        @LayoutRes
        get() = 0

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, defStyleRes))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (layoutToInflate != 0)
            View.inflate(context, layoutToInflate, this)
        prepareView()
    }

    protected open fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()


}

/**
 * Created by oleg on 23.12.16.
 */

abstract class BaseTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val styleableResource = getStyleable(context)
        if (styleableResource != null)
            init(context.obtainStyledAttributes(attrs, styleableResource, defStyleAttr, 0))
    }

    private fun init(array: TypedArray) {
        onTypedArrayReady(array)
        array.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareView()
    }

    protected fun onTypedArrayReady(array: TypedArray) {}

    protected abstract fun prepareView()
}