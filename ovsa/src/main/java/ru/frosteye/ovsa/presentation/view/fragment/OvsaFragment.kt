package ru.frosteye.ovsa.presentation.view.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.ArrayRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.text.Editable
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import ru.frosteye.ovsa.presentation.callback.SimpleTextChangeCallback
import ru.frosteye.ovsa.presentation.view.activity.OvsaActivity
import ru.frosteye.ovsa.stub.impl.SimpleTextWatcher
import ru.frosteye.ovsa.stub.listener.OnDoneListener
import ru.frosteye.ovsa.stub.listener.SelectListener
import ru.frosteye.ovsa.tool.UITools
import java.util.*

/**
 * Created by oleg on 27.06.16.
 */
open class OvsaFragment : Fragment() {

    private val textChangeListeners = HashMap<SimpleTextWatcher, (Editable) -> Unit>()

    protected open val fragmentLayout: Int
        get() = 0


    protected fun registerTextChangeListeners(changeCallback: (Editable) -> Unit, vararg views: TextView) {
        val textWatcher = object : SimpleTextWatcher() {
            override fun afterTextChanged(editable: Editable) {
                textChangeListeners[this]?.invoke(editable)
            }
        }
        for (view in views) {
            view.addTextChangedListener(textWatcher)
        }
        textChangeListeners[textWatcher] = changeCallback
    }

    protected fun openUrl(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return if (fragmentLayout != 0) inflater.inflate(fragmentLayout, container, false) else super.onCreateView(inflater, container, savedInstanceState)
    }

    protected fun setOnDoneListener(textView: TextView, onDoneListener: () -> Unit) {
        textView.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                onDoneListener.invoke()
            }
            false
        }
    }

    fun startActivity(clazz: Class<out Activity>) {
        val intent = Intent(activity, clazz)
        startActivity(intent)
    }

    fun startActivity(clazz: Class<out Activity>, vararg payloads: Parcelable?) {

        startActivity(Intent(activity, clazz).apply {
            payloads.forEachIndexed { i, p ->
                p?.let {
                    val name = p::class.java.name
                    putExtra(name, p)
                }
            }
        })
    }

    fun startActivityForResult(clazz: Class<out Activity>, code: Int, vararg payloads: Parcelable?) {
        val intent = Intent(activity, clazz)
        payloads.forEachIndexed { i, p ->
            p?.let {
                val name = p::class.java.name
                intent.putExtra(name, p)
            }
        }
        startActivityForResult(intent, code)
    }

    fun startActivity(clazz: Class<out Activity>, items: Map<String, Parcelable>) {
        val intent = Intent(activity, clazz)
        items.forEach {
            intent.putExtra(it.key, it.value)

        }
        startActivity(intent)
    }


    protected fun showTopBarLoading(loading: Boolean) {
        if (activity != null && activity is OvsaActivity) {
            (activity as OvsaActivity).showTopBarLoading(loading)
        }
    }

    @JvmOverloads
    protected fun showSelect(context: Context,
                             @ArrayRes res: Int,
                             listener: SelectListener, notCancelable: Boolean = false) {
        val titles = context.resources.getStringArray(res)
        showSelect(context, titles, listener, notCancelable)
    }

    protected fun showSelect(context: Context,
                             items: Array<String>,
                             listener: SelectListener, notCancelable: Boolean) {
        val builder = AlertDialog.Builder(context)
        builder.setItems(items) { dialogInterface, i -> listener.onSelect(items[i], i) }
        builder.setCancelable(!notCancelable)
        builder.show()
    }

    fun hideKeyboard() {
        try {
            val view = activity!!.currentFocus
            if (view != null) {
                val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        } catch (ignored: Exception) {

        }

    }

    fun showKeyboard(view: EditText) {
        view.requestFocus()
        val keyboard = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboard.showSoftInput(view, 0)
    }

    fun showMessage(message: String?) {
        view?.let {
            Snackbar.make(it, message!!, Snackbar.LENGTH_LONG).show()
        } ?: run {
            UITools.toastLong(context, message)
        }
    }

    fun showError(error: String?) {
        view?.let {
            Snackbar.make(it, error!!, Snackbar.LENGTH_LONG).show()
        } ?: run {
            UITools.toastLong(context, error)
        }
    }

    fun showToast(message: String?) {
        view?.let {
            Snackbar.make(it, message!!, Snackbar.LENGTH_LONG).show()
        } ?: run {
            UITools.toastLong(context, message)
        }
    }

    fun showSuccess(message: String?) {
        view?.let {
            Snackbar.make(it, message!!, Snackbar.LENGTH_LONG).show()
        } ?: run {
            UITools.toastLong(context, message)
        }
    }



    open fun showMessage(message: CharSequence?, code: Int) {
        when (code) {
            -1 -> showError(message.toString())
            0 -> showToast(message.toString())
            else -> showSuccess(message.toString())
        }
    }
}
