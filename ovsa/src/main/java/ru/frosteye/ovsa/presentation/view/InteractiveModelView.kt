package ru.frosteye.ovsa.presentation.view

/**
 * Created by oleg on 17.08.17.
 */

interface InteractiveModelView<T> : ModelView<T> {
    var listener: Listener?

    interface Listener {
        fun onModelAction(code: Int, payload: Any? = null)
    }
}

interface StrictInteractiveModelView<T> : StrictModelView<T> {
    var listener: Listener?

    interface Listener {
        fun onModelAction(code: Int, payload: Any? = null)
    }
}


