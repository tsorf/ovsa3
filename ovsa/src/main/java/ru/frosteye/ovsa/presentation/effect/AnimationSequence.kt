package ru.frosteye.ovsa.presentation.effect

import android.view.View
import android.view.animation.Animation

import java.util.ArrayDeque
import java.util.ArrayList
import java.util.Deque

import ru.frosteye.ovsa.stub.impl.SimpleAnimationListener

/**
 * Created by oleg on 17.08.16.
 */
class AnimationSequence {
    private val animations = ArrayDeque<Animation>()
    private val backUp = ArrayList<Animation>()
    private lateinit var listener: Listener
    var isRepeat = false

    fun addAnimation(animation: Animation) {
        this.animations.add(animation)
        this.backUp.add(animation)
    }

    fun start(view: View, listener: Listener) {
        this.listener = listener
        reset()
        run(view, animations.poll())
    }

    private fun run(view: View, animation: Animation?) {
        if (animation == null) {
            listener!!.onComplete()
            if (isRepeat)
                start(view, listener)
            return
        }
        animation.setAnimationListener(object : SimpleAnimationListener() {
            override fun onAnimationEnd(animation: Animation) {
                run(view, animations.poll())
            }
        })
        view.startAnimation(animation)
    }

    fun reset() {
        animations.clear()
        for (animation in backUp) {
            animation.reset()
            animations.add(animation)
        }
    }

    interface Listener {
        fun onComplete()
    }
}
