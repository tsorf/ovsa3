package ru.frosteye.ovsa.presentation.view.widget

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.util.AttributeSet

/**
 * Created by ovcst on 30.05.2018.
 */
class RefreshableSwipeRefreshLayout : SwipeRefreshLayout {
    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    override fun setRefreshing(refreshing: Boolean) {
        if (!refreshing) {
            post { super@RefreshableSwipeRefreshLayout.setRefreshing(false) }
        } else {
            post { super@RefreshableSwipeRefreshLayout.setRefreshing(true) }
        }
    }
}