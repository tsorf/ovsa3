package ru.frosteye.ovsa.presentation.view

/**
 * Created by user on 03.07.16.
 */
interface ModelView<T> {
    var model: T?
}

interface ConsumerView<T> : BasicView {
    var model: T?
}

interface StrictModelView<T> {
    var model: T
}
