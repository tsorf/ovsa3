package ru.frosteye.ovsa.presentation.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import ru.frosteye.ovsa.presentation.view.ModelView

/**
 * Created by oleg on 12.07.17.
 */

abstract class AdapterItem<T, V> : AbstractFlexibleItem<ModelViewHolder<V>> where V : View, V : ModelView<T> {

    var model: T? = null
    abstract val layoutResource: Int

    constructor(model: T) {
        this.model = model
    }

    constructor() {}

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as AdapterItem<*, *>?

        return if (model != null) model == that!!.model else that!!.model == null

    }

    open override fun getLayoutRes(): Int {
        return layoutResource
    }

    override fun hashCode(): Int {
        return if (model != null) model!!.hashCode() else 0
    }

    override fun createViewHolder(
        view: View,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ): ModelViewHolder<V> {
        val holder = ModelViewHolder<V>(view, adapter)
        if (adapter is FlexibleModelAdapter<*>) {
            holder.setListener(adapter.listener)
        }
        return holder
    }
    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<*>>, holder: ModelViewHolder<V>,
        position: Int, payloads: List<Any>
    ) {
        holder.view.model = model
    }
}

abstract class StaticAdapterItem : AbstractFlexibleItem<StaticFlexibleViewHolder>() {

    abstract val layoutResource: Int



    open override fun getLayoutRes(): Int {
        return layoutResource
    }

    override fun createViewHolder(
            view: View,
            adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ): StaticFlexibleViewHolder {
        return StaticFlexibleViewHolder(view, adapter)
    }



    override fun bindViewHolder(
            adapter: FlexibleAdapter<IFlexible<*>>, holder: StaticFlexibleViewHolder,
            position: Int, payloads: List<Any>
    ) {
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as StaticAdapterItem

        if (layoutResource != other.layoutResource) return false

        return true
    }

    override fun hashCode(): Int {
        return layoutResource
    }
}
