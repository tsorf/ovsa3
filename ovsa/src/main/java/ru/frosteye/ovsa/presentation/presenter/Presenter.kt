package ru.frosteye.ovsa.presentation.presenter

/**
 * Created by oleg on 15.06.16.
 */
interface Presenter<T> {
    fun onAttach(v: T, vararg params: Any)
}
