package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import ru.frosteye.ovsa.R;


/**
 * Created by ovcst on 06.05.2018.
 */

public class CompatTextView extends AppCompatTextView {

    private Drawable drawableStart = null;
    private Drawable drawableEnd = null;
    private Drawable drawableBottom = null;
    private Drawable drawableTop = null;

    public CompatTextView(Context context) {
        super(context);
    }

    public CompatTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    public CompatTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
    }

    void initAttrs(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray attributeArray = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.CompatTextView);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableStart = attributeArray.getDrawable(R.styleable.CompatTextView_drawableStartCompat);
                drawableEnd = attributeArray.getDrawable(R.styleable.CompatTextView_drawableEndCompat);
                drawableBottom = attributeArray.getDrawable(R.styleable.CompatTextView_drawableBottomCompat);
                drawableTop = attributeArray.getDrawable(R.styleable.CompatTextView_drawableTopCompat);
            } else {
                final int drawableStartId = attributeArray.getResourceId(R.styleable.CompatTextView_drawableStartCompat, -1);
                final int drawableEndId = attributeArray.getResourceId(R.styleable.CompatTextView_drawableEndCompat, -1);
                final int drawableBottomId = attributeArray.getResourceId(R.styleable.CompatTextView_drawableBottomCompat, -1);
                final int drawableTopId = attributeArray.getResourceId(R.styleable.CompatTextView_drawableTopCompat, -1);

                if (drawableStartId != -1)
                    drawableStart = AppCompatResources.getDrawable(context, drawableStartId);
                if (drawableEndId != -1)
                    drawableEnd = AppCompatResources.getDrawable(context, drawableEndId);
                if (drawableBottomId != -1)
                    drawableBottom = AppCompatResources.getDrawable(context, drawableBottomId);
                if (drawableTopId != -1)
                    drawableTop = AppCompatResources.getDrawable(context, drawableTopId);
            }

            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                setCompoundDrawablesWithIntrinsicBounds(drawableStart, drawableTop, drawableEnd, drawableBottom);
            }

            attributeArray.recycle();
        }
    }

    public void setDrawableLeft(@DrawableRes int drawable) {
        Drawable dr = AppCompatResources.getDrawable(getContext(), drawable);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            setCompoundDrawablesWithIntrinsicBounds(dr, drawableTop, drawableEnd, drawableBottom);
        }
    }
}
