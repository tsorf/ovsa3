package ru.frosteye.ovsa.tool.picture;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


public class WatermarkCreator {
    private File file;

    public WatermarkCreator(File file) {
        this.file = file;
    }

    public File mark(String text) throws FileNotFoundException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(15f);
        paint.setColor(Color.WHITE);
        paint.setAlpha(128);
        canvas.drawText(text, 20, canvas.getHeight() - 20, paint);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, new FileOutputStream(file));
        return file;
    }
}
