package ru.frosteye.ovsa.tool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by user on 22.07.16.
 */
public class ImageTools {
    public static File drawableToFile(Context context, Drawable drawable) {
        try {
            String path = Environment.getExternalStorageDirectory().getPath() +
                    "/Android/data/" + context.getPackageName() + "/.images/";
            File parent = new File(path);
            if(!parent.exists()) parent.mkdirs();
            Bitmap bitmap = drawableToBitmap(drawable);
            File file = new File(parent, context.getString(context.getApplicationInfo().labelRes) + System.currentTimeMillis() + ".jpg");
            FileOutputStream outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
            return file;
        } catch (Exception e) {
            return null;
        }
    }
    public static File bitmapToFile(Context context, Bitmap bitmap) {
        try {
            String path = Environment.getExternalStorageDirectory().getPath() +
                    "/Android/data/" + context.getPackageName() + "/.images/";
            File parent = new File(path);
            if(!parent.exists()) parent.mkdirs();
            File file = new File(parent, context.getString(context.getApplicationInfo().labelRes) + System.currentTimeMillis() + ".jpg");
            FileOutputStream outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.RGB_565);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
