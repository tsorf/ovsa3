package ru.frosteye.ovsa.tool

import android.content.Context
import android.content.Intent
import android.net.Uri


fun goToMarket(context: Context, packageName: String? = null) {
    val pack = if (packageName != null) packageName else context.packageName
    try {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$pack")))
    } catch (anfe: android.content.ActivityNotFoundException) {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$pack")))
    }

}