package ru.frosteye.ovsa.tool;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

/**
 * Created by oleg on 01.06.17.
 */

public class TimeCounter {

    public interface Listener {
        void onCounter(String time, int seconds);
    }

    @Inject
    public TimeCounter() {
    }

    private volatile int seconds = 0;
    private Listener listener;
    private Timer timer;

    private void incrementAndNotify() {
        seconds++;
        listener.onCounter(DateTools.INSTANCE.formatSecondsToTime(seconds), seconds);
    }

    private void decrementAndNotify() {
        if(seconds == 0) return;
        seconds--;
        listener.onCounter(DateTools.INSTANCE.formatSecondsToTime(seconds), seconds);
    }

    public void start(Listener listener, int initialSeconds, final boolean inverse) {
        if(inverse && initialSeconds < 0)
            throw new RuntimeException("seconds < 0");
        this.listener = listener;
        this.seconds = initialSeconds;
        if(timer != null) {
            try {
                timer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(inverse) {
                    decrementAndNotify();
                } else {
                    incrementAndNotify();
                }
            }
        }, 0, 1000);
    }

    public int getSeconds() {
        return seconds;
    }

    public void stop() {
        seconds = 0;
        try {
            timer.cancel();
            timer = null;
        } catch (Exception ignored) {

        }
    }
}
