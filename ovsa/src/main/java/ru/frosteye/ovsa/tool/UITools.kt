package ru.frosteye.ovsa.tool

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.hardware.Camera
import android.support.annotation.ArrayRes
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.Display
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast

import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Field

import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.presentation.view.IPrompt
import ru.frosteye.ovsa.stub.listener.SelectListener

/**
 * Created by oleg on 15.06.16.
 */
object UITools {

    fun Activity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun toastLong(context: Context?, message: CharSequence?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @JvmStatic
    fun toastLong(context: Context?, message: Int?) {
        if (context == null || message == null) return
        toastLong(context, context.getString(message))
    }

    fun toastShort(context: Context?, message: CharSequence?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun toastShort(context: Context?, message: Int?) {
        if (context == null || message == null) return
        toastShort(context, context.getString(message))
    }

    @JvmOverloads
    fun simpleAlert(
            context: Context, title: String, message: String,
            listener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialogInterface, i -> }
    ): AlertDialog {
        return AlertDialog.Builder(context).setTitle(title).setMessage(message)
                .setPositiveButton(R.string.ovsa_string_ok, listener).show()
    }

    fun simpleAlert(context: Context, title: Int, message: Int): AlertDialog {
        return simpleAlert(
                context,
                context.getString(title),
                context.getString(message),
                DialogInterface.OnClickListener { dialogInterface, i -> })
    }

    fun simpleAlert(
            context: Context, title: Int, message: Int,
            listener: DialogInterface.OnClickListener
    ): AlertDialog {
        return simpleAlert(context, context.getString(title), context.getString(message), listener)
    }

    fun simpleAlert(context: Context, title: Int, message: String): AlertDialog {
        return simpleAlert(
                context,
                context.getString(title),
                message,
                DialogInterface.OnClickListener { dialogInterface, i -> })
    }

    fun getResourceDeclareStyleableIntArray(context: Context, name: String): IntArray? {
        try {
            //use reflection to access the resource class
            val fields2 = Class.forName(context.packageName + ".R\$styleable").fields

            //browse all fields
            for (f in fields2) {
                //pick matching field
                if (f.name == name) {
                    //return as int array
                    return f.get(null) as IntArray
                }
            }
        } catch (t: Throwable) {
        }

        return null
    }

    fun simpleAlert(context: Context, title: String, message: Int): AlertDialog {
        return simpleAlert(
                context,
                title,
                context.getString(message),
                DialogInterface.OnClickListener { dialogInterface, i -> })
    }

    @JvmStatic
    fun applyDialogLayoutDimensions(dialog: Dialog, width: Int, height: Int) {
        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        lp.width = width
        lp.height = height
        window.attributes = lp
    }

    fun selector(
            context: Context, @ArrayRes array: Int,
            selectListener: SelectListener
    ) {
        selector(context, context.resources.getStringArray(array), selectListener)
    }

    fun selector(
            context: Context, @ArrayRes array: Int,
            selectListener: (String, Int) -> Unit
    ) {
        selector(context, context.resources.getStringArray(array), selectListener)
    }

    fun selector(
            context: Context, array: Array<String>,
            selectListener: SelectListener
    ) {
        AlertDialog.Builder(context).setItems(array) { dialogInterface, i -> selectListener.onSelect(array[i], i) }
                .show()
    }

    fun selector(
            context: Context, array: Array<String>,
            selectListener: (String, Int) -> Unit
    ) {
        AlertDialog.Builder(context).setItems(array) { dialogInterface, i -> selectListener(array[i], i) }
                .show()
    }

    fun selector(
            context: Context, title: String, items: Collection<String>,
            cancelable: Boolean = true,
            selectListener: (String, Int) -> Unit
    ) {
        val array = items.toTypedArray()
        AlertDialog.Builder(context).setTitle(title)
                .setItems(array) { dialogInterface, i -> selectListener.invoke(array[i], i) }
                .setCancelable(cancelable)
                .show()
    }

    fun selector(
            context: Context, title: String, array: Array<String>,
            selectListener: (String, Int) -> Unit
    ) {
        AlertDialog.Builder(context).setTitle(title)
                .setItems(array) { dialogInterface, i -> selectListener.invoke(array[i], i) }.show()
    }

    fun selector(
            context: Context, title: String, @ArrayRes array: Int,
            selectListener: (String, Int) -> Unit
    ) {
        val items = context.resources.getStringArray(array)
        AlertDialog.Builder(context).setTitle(title)
                .setItems(items) { dialogInterface, i -> selectListener.invoke(items[i], i) }.show()
    }

    fun showPrompt(
            context: Activity, prompt: IPrompt,
            listener: IPrompt.Listener
    ) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(prompt.title())
        val view = LayoutInflater.from(context).inflate(R.layout.view_prompt, null)
        val input = view.findViewById<View>(R.id.view_prompt_input) as EditText
        input.hint = prompt.hint()
        builder.setView(view)
        if (prompt.inputType() != 0) {
            input.inputType = prompt.inputType()
            if (prompt.inputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD
                    || prompt.inputType() == InputType.TYPE_NUMBER_VARIATION_PASSWORD
            ) {
                input.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }
        input.setText(prompt.text())
        builder.setCancelable(prompt.cancelable())
        builder.setNegativeButton(if (prompt.negativeButton() == null) context.getString(R.string.ovsa_string_cancel) else prompt.negativeButton()) { dialog, which ->
            dialog.cancel()
            listener.onCancel()
        }
        builder.setPositiveButton(prompt.positiveButton(), DialogInterface.OnClickListener { dialog, which ->
            if (input.isTrimmedEmpty()) {
                listener.onCancel()
                return@OnClickListener
            }
            listener.onResult(input.trimmed())
            dialog.cancel()
        })
        builder.show()
    }


    fun selectorNonCancelable(
            context: Context, title: String, array: Array<String>,
            selectListener: SelectListener
    ) {
        AlertDialog.Builder(context).setTitle(title).setCancelable(false)
                .setItems(array) { dialogInterface, i -> selectListener.onSelect(array[i], i) }.show()
    }

    fun confirm(
            context: Context, title: Int,
            simpleConfirmCallback: SimpleConfirmCallback
    ) {
        confirm(context, context.getString(title), null, simpleConfirmCallback)
    }

    fun confirm(
            context: Context, title: Int, message: Int,
            simpleConfirmCallback: SimpleConfirmCallback
    ) {
        confirm(context, context.getString(title), context.getString(message), simpleConfirmCallback)
    }

    fun confirm(
            context: Context, title: String, message: String?,
            simpleConfirmCallback: SimpleConfirmCallback
    ) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        if (message != null) builder.setMessage(message)
        builder.setPositiveButton(R.string.ovsa_string_ok) { dialogInterface, i -> simpleConfirmCallback.yes() }
        if (simpleConfirmCallback is ConfirmCallback) {
            builder.setNegativeButton(R.string.ovsa_string_cancel) { dialogInterface, i -> simpleConfirmCallback.no() }
        }
        builder.show()
    }

    fun setCameraDisplayOrientation(activity: Activity, camera: Camera) {
        var cameraId = -1
        val numberOfCameras = Camera.getNumberOfCameras()
        for (i in 0 until numberOfCameras) {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(i, info)
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i
                break
            }
        }
        if (cameraId == -1) return
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)
        val rotation = activity.windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }

        var result: Int
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360
            result = (360 - result) % 360  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360
        }
        camera.setDisplayOrientation(result)
    }

    fun saveBitmapToJpgFile(directory: File, name: String, bitmap: Bitmap): File? {
        try {
            if (!directory.exists()) directory.mkdirs()
            val file = File(directory, "$name.jpg")
            val fileOutputStream = FileOutputStream(file)

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()
            return file
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    fun resizeBitmap(context: Context, drawableName: String, width: Int, height: Int): Bitmap {
        val imageBitmap = BitmapFactory.decodeResource(
                context.resources,
                context.resources.getIdentifier(drawableName, "drawable", context.packageName)
        )
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    fun getWindowDimens(activity: Activity): Point {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size
    }

    interface SimpleConfirmCallback {
        fun yes()
    }

    interface ConfirmCallback : SimpleConfirmCallback {
        fun no()
    }
}

fun Dialog.applyLayoutDimensions(width: Int, height: Int) {
    val lp = WindowManager.LayoutParams()
    window?.let {
        lp.copyFrom(it.attributes)
        lp.width = width
        lp.height = height
        it.attributes = lp
    }
}
