package ru.frosteye.ovsa.stub.listener;

/**
 * Created by oleg on 28.12.2017.
 */

@Deprecated
public interface CommonListener<T> {
    void onResult(T result);
}
