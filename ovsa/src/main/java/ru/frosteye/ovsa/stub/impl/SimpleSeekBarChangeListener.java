package ru.frosteye.ovsa.stub.impl;

import android.widget.SeekBar;

/**
 * Created by oleg on 05.12.16.
 */

public class SimpleSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {
    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
