package ru.frosteye.ovsa.stub.listener;

/**
 * Created by user on 03.07.16.
 */
public interface SelectListener {
    void onSelect(String text, int position);
}
