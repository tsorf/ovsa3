package ru.frosteye.ovsa.stub.impl

import android.text.Editable
import android.text.TextWatcher

/**
 * Created by oleg on 21.06.16.
 */
open class SimpleTextWatcher : TextWatcher {
    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun afterTextChanged(editable: Editable) {

    }
}
