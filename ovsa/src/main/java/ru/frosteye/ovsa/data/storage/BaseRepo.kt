package ru.frosteye.ovsa.data.storage

/**
 * Created by ovcst on 08.06.2017.
 */
@Deprecated("use Repository")
abstract class BaseRepo<T>(protected var storage: Storage) : Repo<T> {
    protected var model: T? = null

    @Synchronized
    override fun load(): T? {
        if (model == null) {
            model = storage.get(provideClass().name, provideClass())
        }
        if (model == null) {
            model = handleNull()
        }
        return model
    }

    @Synchronized
    override fun executeAndSave(runnable: Repo.RepoRunnable<T>) {
        val `object` = load()
        runnable.run(`object`)
        save(`object`)
    }

    protected open fun handleNull(): T? {
        return null
    }

    protected abstract fun provideClass(): Class<T>

    @Synchronized
    override fun save(t: T?) {
        this.model = t
        storage.save(provideClass().name, model)
    }
}
