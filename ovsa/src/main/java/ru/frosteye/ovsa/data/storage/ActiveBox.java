package ru.frosteye.ovsa.data.storage;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by oleg on 31.08.17.
 */

@Singleton @Deprecated
public class ActiveBox {

    public static final String PREFIX = "_active";

    private final Map<String, Object> activies = new HashMap<>();
    private Storage storage;

    @Inject
    public ActiveBox(Storage storage) {
        this.storage = storage;
    }

    public <T> T getActive(Class<T> clazz) {
        synchronized (activies) {
            Object object = activies.get(PREFIX + clazz.getName());
            if(object == null) {
                return get(PREFIX + clazz.getName(), clazz);
            } else {
                return clazz.cast(object);
            }
        }

    }

    public void setActive(Object object) {
        synchronized (activies) {
            activies.put(PREFIX + object.getClass().getName(), object);
            save(object);
        }
    }

    private void save(Object object) {
        storage.save(PREFIX + object.getClass().getName(), object);
    }

    private<T> T get(String key, Class<T> clazz) {
        return storage.get(PREFIX + clazz.getName(), clazz);
    }

    public void clear(Class<?> clazz) {
        synchronized (activies) {
            storage.save(PREFIX + clazz.getName(), null);
            activies.remove(PREFIX + clazz.getName());
        }
    }
}
