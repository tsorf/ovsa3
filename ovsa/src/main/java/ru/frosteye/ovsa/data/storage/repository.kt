package ru.frosteye.ovsa.data.storage

import io.reactivex.Observable
import java.lang.RuntimeException
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

const val REPO_CODE_UNKNOWN = -1
const val REPO_CODE_EMPTY = 0
const val REPO_CODE_LOCAL = 1
const val REPO_CODE_REMOTE = 2

interface CommonRepository : Repository<RepoIndex>

interface Repository<T> : ReadWriteProperty<Any?, T?> {

    fun load(): Observable<RepoData<T>>

    fun save(data: T?)

    fun executeAndSave(runnable: (RepoData<T>) -> Unit)

    fun addListener(listener: Listener<T>)

    fun removeListener(listener: Listener<T>)

    interface Listener<T> {

        fun onUpdate(data: T?)
    }
}

data class RepoData<T>(
        val data: T?,
        val code: Int? = REPO_CODE_UNKNOWN
)

class RepoIndex {
    var state: Int = 0
}

abstract class IndexRepository(storage: Storage) : BaseRepository<RepoIndex>(storage) {

    override val clazz: Class<RepoIndex> = RepoIndex::class.java

    override fun handleNull(): RepoIndex? = RepoIndex()

    final override fun save(data: RepoIndex?) {
        throw RuntimeException("index rewriting is not allowed in IndexRepository")
    }
}

abstract class BaseRepository<T>(protected var storage: Storage) : Repository<T> {

    protected var model: T? = null
    protected abstract val clazz: Class<T>
    private val listeners: MutableSet<Repository.Listener<T>> = mutableSetOf()

    @Synchronized
    override fun load(): Observable<RepoData<T>> {
        return createSource()
    }

    protected open fun loadLocal(): T? {
        if (model == null) {
            model = storage.get(clazz.name, clazz)
        }
        if (model == null) {
            model = handleNull()
        }
        return model
    }

    @Synchronized
    override fun executeAndSave(runnable: (RepoData<T>) -> Unit) {
        val data = loadLocal()
        val pack = RepoData(data, if (data == null) REPO_CODE_EMPTY else REPO_CODE_LOCAL)
        runnable.invoke(pack)
        save(pack.data)
    }

    protected open fun createSource(): Observable<RepoData<T>> {
        val data = loadLocal()
        return Observable.just(RepoData(data,
                if (data == null) REPO_CODE_EMPTY else REPO_CODE_LOCAL))
    }

    protected open fun handleNull(): T? {
        return null
    }

    @Synchronized
    override fun save(data: T?) {
        this.model = data
        storage.save(clazz.name, model)
        synchronized(listeners) {
            listeners.forEach { it.onUpdate(data) }
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return loadLocal()
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        save(value)
    }

    override fun addListener(listener: Repository.Listener<T>) {
        synchronized(listeners) {
            listeners.add(listener)
        }
    }

    override fun removeListener(listener: Repository.Listener<T>) {
        synchronized(listeners) {
            listeners.remove(listener)
        }
    }
}
