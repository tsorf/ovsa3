package ru.frosteye.ovsa.data.entity

/**
 * Created by oleg on 15.12.2017.
 */

class GeocodedCity(val name: String, val lat: Double, val lng: Double) {

    override fun toString(): String {
        return name
    }
}
