package ru.frosteye.ovsa.data.entity

import android.location.Location

import java.io.Serializable

/**
 * Created by oleg on 28.12.16.
 */

open class SimpleLocation(
        val lat: Double,
        val lng: Double
) : Serializable {

    constructor(location: Location) : this(location.latitude, location.longitude)
}
