package ru.frosteye.ovsa.data.storage

import android.content.SharedPreferences

import java.util.HashSet

import javax.inject.Inject

import ru.frosteye.ovsa.execution.serialization.Serializer


class SharedPreferencesStorage @Inject
constructor(private val sharedPreferences: SharedPreferences, private val serializer: Serializer) : Storage {

    override fun save(key: String, `object`: Any?) {
        var `object` = `object`
        if (isPrimitive(`object`)) {
            `object` = `object`.toString()
        }
        sharedPreferences.edit().putString(key,
                if (`object` != null) serializer.serialize(`object`)  else null).commit()
    }

    override fun <T> get(key: String, clazz: Class<T>): T? {
        return serializer.deserialize(sharedPreferences.getString(key, null), clazz)
    }

    override fun serializer(): Serializer {
        return serializer
    }

    companion object {

        private val PRIMITIVES: MutableSet<Class<*>>

        init {
            PRIMITIVES = HashSet()
            PRIMITIVES.add(Boolean::class.java)
            PRIMITIVES.add(Char::class.java)
            PRIMITIVES.add(Byte::class.java)
            PRIMITIVES.add(Short::class.java)
            PRIMITIVES.add(Int::class.java)
            PRIMITIVES.add(Long::class.java)
            PRIMITIVES.add(Float::class.java)
            PRIMITIVES.add(Double::class.java)
            PRIMITIVES.add(Void::class.java)
        }

        private fun isPrimitive(`object`: Any?): Boolean {
            return if (`object` == null) false else PRIMITIVES.contains(`object`.javaClass)
        }
    }
}
