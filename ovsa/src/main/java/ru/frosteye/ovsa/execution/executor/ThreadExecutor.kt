package ru.frosteye.ovsa.execution.executor

import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

import javax.inject.Inject

/**
 * Created by oleg on 14.06.16.
 */
@Deprecated("shit")
class ThreadExecutor @Inject
constructor() : Executor {
    internal val threadPoolExecutor: ExecutorService

    init {
        this.threadPoolExecutor = Executors.newFixedThreadPool(MAX_THREAD_POOL_COUNT)
    }

    override fun execute(command: Runnable) {
        threadPoolExecutor.execute(command)
    }

    companion object {

        private val MAX_THREAD_POOL_COUNT = 8
    }
}
