package ru.frosteye.ovsa.execution.network.request;

import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by user on 02.07.16.
 */
public class MultipartRequestParams extends HashMap<String, RequestBody> {

    public MultipartRequestParams addPart(String key, String value) {
        super.put(key, RequestBody.create(MediaType.parse("text/plain"), value == null ? "" : value));
        return this;
    }

    public MultipartRequestParams addPart(String key, String value, boolean json) {
        if (json) {
            super.put(key, RequestBody.create(MediaType.parse("application/json"), value == null ? "{}" : value));
        } else {
            addPart(key, value);
        }
        return this;
    }

    public MultipartRequestParams addPart(String key, int value) {
        super.put(key, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value)));
        return this;
    }

    public MultipartRequestParams addPart(String key, double value) {
        super.put(key, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value)));
        return this;
    }

    public MultipartRequestParams addPart(String key, boolean value) {
        super.put(key, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value)));
        return this;
    }

    public MultipartRequestParams addPart(String key, File file) {
        try {
            String ext = MimeTypeMap.getFileExtensionFromUrl(file.getPath());
            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
            MediaType type = MediaType.parse(mime);

            super.put(String.format("%s\"; filename=\"%s", key, file.getName()), RequestBody.create(type, file));
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            super.put(String.format("%s\"; filename=\"%s", key, file.getName()), RequestBody.create(MediaType.parse("multipart/form-data"), file));
            return this;
        }

    }

    @Override
    public RequestBody put(String key, RequestBody value) {
        throw new RuntimeException("Operation is not supported. Use addPart() instead");
    }

    @Override
    public void putAll(Map<? extends String, ? extends RequestBody> m) {
        throw new RuntimeException("Operation is not supported. Use addPart() instead");
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(Entry<String,RequestBody> entry: entrySet()) {
            builder.append(entry.getKey()).append(": ").append(entry.getValue().toString()).append("\n");
        }
        return builder.toString();
    }
}
