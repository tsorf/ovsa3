package ru.frosteye.ovsa.execution.executor

/**
 * Created by oleg on 22.09.17.
 */

interface Callback<T> {
    fun onResult(result: T)
}
