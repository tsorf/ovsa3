package ru.frosteye.ovsa.execution.network.client

import com.google.gson.*

import java.io.IOException
import java.util.ArrayList
import java.util.HashMap
import java.util.concurrent.TimeUnit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.frosteye.ovsa.BuildConfig
import ru.frosteye.ovsa.data.constant.OvsaKeys
import java.lang.reflect.Type

/**
 * Created by oleg on 28.12.16.
 */

abstract class BaseRetrofitClient<T : Any> {

    lateinit var api: T
    open var identityProvider: IdentityProvider? = null

    protected var token: String? = null
        get() = if (identityProvider != null) {
            identityProvider!!.provideIdentity()
        } else field
        set

    protected val connectTimeout: Int
        get() = 30

    protected val readTimeout: Int
        get() = 30

    protected open val authHeaderName: String
        get() = OvsaKeys.AUTHORIZATION

    protected open val authHeaderValuePrefix: String
        get() = ""

    open val headers: MutableMap<String, String> = mutableMapOf()

    constructor(baseUrl: String) {
        init(baseUrl)
    }

    constructor(baseUrl: String, identityProvider: IdentityProvider) {
        init(baseUrl, identityProvider)
    }

    protected open fun init(baseUrl: String, identityProvider: IdentityProvider? = null) {
        this.identityProvider = identityProvider
        val clientBuilder = OkHttpClient.Builder().apply {
            createClientInterceptors().forEach {
                this.addInterceptor(it)
            }
            connectTimeout(connectTimeout.toLong(), TimeUnit.SECONDS)
            readTimeout(readTimeout.toLong(), TimeUnit.SECONDS)
        }



        val gson = createGsonBuilder()
                .setExclusionStrategies(TransientExclusionStrategy)
                .create()

        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(clientBuilder.build())

        populateRetrofitBuilder(builder)
        builder.addConverterFactory(GsonConverterFactory.create(gson))


        createCallAdapters().forEach {
            builder.addCallAdapterFactory(it)
        }

        this.api = builder.build().create(apiClass())
    }



    protected open fun populateRetrofitBuilder(builder: Retrofit.Builder) {}

    abstract fun apiClass(): Class<T>

    protected open fun createGsonBuilder(): GsonBuilder {
        return GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
    }

    protected open fun createClientInterceptors(): MutableList<Interceptor> {
        val interceptors = ArrayList<Interceptor>()
        interceptors.add(Interceptor { chain ->
            val builder = chain.request().newBuilder()
            if (token != null) {
                builder.addHeader(authHeaderName, authHeaderValuePrefix + token!!)
            }
            for ((key, value) in headers) {
                builder.addHeader(key, value)
            }
            chain.proceed(builder.build())
        })
        if (BuildConfig.DEBUG)
            interceptors.add(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        return interceptors
    }

    protected fun createCallAdapters(): List<CallAdapter.Factory> {
        val adapters = ArrayList<CallAdapter.Factory>()
        adapters.add(RxJava2CallAdapterFactory.create())
        return adapters
    }
}

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY)
annotation class GsonTransient

object TransientExclusionStrategy : ExclusionStrategy {
    override fun shouldSkipClass(type: Class<*>): Boolean = false
    override fun shouldSkipField(f: FieldAttributes): Boolean =
            f.getAnnotation(GsonTransient::class.java) != null
                    || f.name.endsWith("\$delegate")
}

class BooleanSerializer : JsonSerializer<Boolean>, JsonDeserializer<Boolean> {

    override fun serialize(src: Boolean?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        if (src == null) return JsonNull.INSTANCE
        return JsonPrimitive(if (src) 1 else 0)
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Boolean {
        if (json == null) return false
        val element = json as JsonPrimitive
        return (element.asInt == 1)
    }


}
