package ru.frosteye.ovsa.execution.task

import java.util.concurrent.Executor

import javax.inject.Inject

import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.annotations.NonNull
import ru.frosteye.ovsa.execution.executor.MainThread
import ru.frosteye.ovsa.execution.network.client.FixedKladrClient

/**
 * Created by oleg on 21.09.17.
 */

class LoadCitiesTask @Inject
constructor(mainThread: MainThread,
            executor: Executor,
            private val fixedKladrClient: FixedKladrClient)
    : ObservableTask<String, List<String>>(mainThread, executor) {
    private var limit = 3

    fun setLimit(limit: Int) {
        this.limit = limit
    }

    override fun prepareObservable(string: String): Observable<List<String>> {
        return Observable.create { subscriber ->
            try {
                val cities = fixedKladrClient.getCityNames(string, limit)
                subscriber.onNext(cities)
                subscriber.onComplete()
            } catch (e: Exception) {
                subscriber.onError(e)
            }
        }
    }
}
