package ru.frosteye.ovsa.execution.network.request

/**
 * Created by oleg on 02.12.2017.
 */

data class PaginatedRequest(
        var page: Int = 1,
        var initialLimit: Int = 15,
        var limit: Int = 15
) {

    fun reset() {
        page = 1
        limit = initialLimit
    }

    fun increase() {
        page++
    }
}
