package ru.frosteye.ovsa.execution.task


import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

/**
 * Created by user on 16.06.16.
 */
open class SimpleSubscriber<T> : Observer<T> {
    override fun onSubscribe(d: Disposable) {

    }

    override fun onComplete() {

    }

    override fun onError(e: Throwable) {

    }

    override fun onNext(t: T) {

    }

    companion object {

        val ERROR = 0
        val SUCCESS = 1
    }
}
