package ru.frosteye.ovsa.execution.network.base

import com.google.gson.Gson

import java.io.IOException
import java.util.concurrent.Executor

import retrofit2.Call
import retrofit2.Response
import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.execution.executor.MainThread
import ru.frosteye.ovsa.execution.network.response.MessageResponse
import ru.frosteye.ovsa.execution.task.ObservableTask

import ru.frosteye.ovsa.data.storage.ResourceHelper.getString

/**
 * Created by ovcst on 07.01.2017.
 */

abstract class NetworkTask<P, Res>(mainThread: MainThread, executor: Executor) : ObservableTask<P, Res>(mainThread, executor) {

    @Throws(Exception::class)
    protected fun <Result> executeCall(call: Call<Result>): Result {
        try {
            val response = call.execute()
            if (response.isSuccessful) return response.body()!!
            val messageResponse = GSON.fromJson(response.errorBody()!!.string(), MessageResponse::class.java)
            throw ApiException(messageResponse.message, messageResponse.code)
        } catch (e: IOException) {
            e.printStackTrace()
            throw ApiException(getString(R.string.default_connection_error), 503)
        }

    }

    companion object {

        protected val GSON = Gson()
    }
}
