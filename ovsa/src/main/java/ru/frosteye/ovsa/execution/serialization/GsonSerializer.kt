package ru.frosteye.ovsa.execution.serialization

import com.google.gson.Gson
import com.google.gson.GsonBuilder

import javax.inject.Inject

/**
 * Created by user on 25.06.16.
 */
class GsonSerializer(private val gson: Gson) : Serializer {

    @Inject
    constructor() : this(GsonBuilder().create())

    override fun serialize(`object`: Any?): String {
        return gson.toJson(`object`)
    }

    override fun <T> deserialize(string: String?, typeOfT: Class<T>): T {
        return gson.fromJson(string, typeOfT)
    }
}
