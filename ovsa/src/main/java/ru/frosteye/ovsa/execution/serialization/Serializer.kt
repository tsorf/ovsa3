package ru.frosteye.ovsa.execution.serialization

/**
 * Created by user on 25.06.16.
 */
interface Serializer {
    /**
     * Perform serialization mechanism on source [Object]
     * @param object [Object] to serialize
     * @return [String], representing source object;
     */
    fun serialize(`object`: Any?): String

    /**
     * Perform deserialization mechanism on source [String] instance.
     * @param string [String] to deserialize
     * @param typeOfT resulting object's type
     * @param <T> any type implementation able to deserialize
     * @return deserialized object
    </T> */
    fun <T> deserialize(string: String?, typeOfT: Class<T>): T
}
