package ru.frosteye.ovsa.execution.network.request;

/**
 * Created by user on 02.07.16.
 */
public interface Requestable {
    MultipartRequestParams createMultipartParams();
}
