package ru.frosteye.ovsa.execution.serialization

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException

import java.lang.reflect.Type

import ru.frosteye.ovsa.presentation.adapter.AdapterItem

/**
 * Created by oleg on 12.07.17.
 */

abstract class AdapterItemDeserializer<T, Wrapper : AdapterItem<T, *>> : JsonDeserializer<Wrapper> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext): Wrapper {

        try {
            val `object` = context.deserialize<T>(json, provideType())
            val wrapper = provideWrapperType().newInstance()
            wrapper.model = `object`
            return wrapper
        } catch (e: Exception) {
            e.printStackTrace()
            throw RuntimeException(e)
        }

    }

    protected abstract fun provideType(): Class<T>
    protected abstract fun provideWrapperType(): Class<Wrapper>
}
