package ru.frosteye.ovsa.execution.executor


/**
 * Created by oleg on 14.06.16.
 */

import io.reactivex.Scheduler

/**
 * Representing Android's UI thread
 */
interface MainThread {
    val scheduler: Scheduler
}
