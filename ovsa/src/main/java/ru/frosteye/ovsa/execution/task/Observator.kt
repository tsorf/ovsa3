package ru.frosteye.ovsa.execution.task

import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.frosteye.ovsa.R
import ru.frosteye.ovsa.data.storage.ResourceHelper.getString
import ru.frosteye.ovsa.execution.network.base.ApiException
import ru.frosteye.ovsa.presentation.view.ConsumerView
import ru.frosteye.ovsa.presentation.view.ModelView
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Created by ovcst on 31.05.2018.
 */
private val threadPoolExecutor: Executor = Executors.newFixedThreadPool(8)
val GSON = Gson()

@Deprecated("use toback() & toFront()")
fun <T : Any> Observable<T>.async(): Observable<T> {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
        .materialize()
        .observeOn(AndroidSchedulers.mainThread())
        .dematerialize()
}

fun <T : Any> Observable<T>.toBack(): Observable<T> {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
}

fun Completable.toBack(): Completable {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
}

fun <T : Any> Observable<T>.toFront(): Observable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun Completable.toFront(): Completable {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun <T : Any> Single<T>.toBack(): Single<T> {
    return this.subscribeOn(Schedulers.from(ru.frosteye.ovsa.execution.task.threadPoolExecutor))
}

fun <T : Any> Single<T>.toFront(): Single<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}


fun <T : Any> Observable<T>.request(result: (T) -> Unit, error: (Throwable) -> Unit): Disposable {
    return this
        .toFront()
        .subscribe({
            result.invoke(it)
        }, {
            try {
                if (it is HttpException) {
                    val resp = GSON.fromJson(
                        it.response().errorBody()?.string(),
                        ru.frosteye.ovsa.execution.network.response.YiiMessageResponse::class.java
                    )
                    error.invoke(
                        ru.frosteye.ovsa.execution.network.base.ApiException(
                            resp.message, resp.code
                        )
                    )
                } else if (it is ApiException) {
                    error(it)
                } else {
                    it.printStackTrace()
                    error.invoke(
                        ru.frosteye.ovsa.execution.network.base.ApiException(
                            getString(R.string.default_connection_error), 0
                        )
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
                error.invoke(
                    ru.frosteye.ovsa.execution.network.base.ApiException(
                        getString(R.string.default_connection_error), 0
                    )
                )
            }

        })
}

fun <T : Any> Single<T>.request(result: (T) -> Unit, error: (Throwable) -> Unit): Disposable {
    return this
        .toFront()
        .subscribe({
            result.invoke(it)
        }, {
            try {
                if (it is HttpException) {
                    val resp = GSON.fromJson(
                        it.response().errorBody()?.string(),
                        ru.frosteye.ovsa.execution.network.response.YiiMessageResponse::class.java
                    )
                    error.invoke(
                        ru.frosteye.ovsa.execution.network.base.ApiException(
                            resp.message, resp.code
                        )
                    )
                } else if (it is ApiException) {
                    error(it)
                } else {
                    it.printStackTrace()
                    error.invoke(
                        ru.frosteye.ovsa.execution.network.base.ApiException(
                            getString(R.string.default_connection_error), 0
                        )
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
                error.invoke(
                    ru.frosteye.ovsa.execution.network.base.ApiException(
                        getString(R.string.default_connection_error), 0
                    )
                )
            }

        })
}

fun <T : Any> Observable<T>.toView(
    view: ConsumerView<T>,
    enableControls: Boolean = true
): Disposable {
    return this
        .toFront()
        .subscribe({
            if (enableControls)
                view.enableControls(true, 0)
            view.model = it
        }, {
            try {
                if (it is HttpException) {
                    val resp = GSON.fromJson(
                        it.response().errorBody()?.string(),
                        ru.frosteye.ovsa.execution.network.response.YiiMessageResponse::class.java
                    )
                    val ex = ru.frosteye.ovsa.execution.network.base.ApiException(
                        resp.message, resp.code
                    )
                    if (enableControls)
                        view.enableControls(true, -1)
                    view.showMessage(ex.message, -1)
                } else {
                    it.printStackTrace()
                    val ex = ru.frosteye.ovsa.execution.network.base.ApiException(
                        getString(R.string.default_connection_error), 0
                    )
                    if (enableControls)
                        view.enableControls(true, -1)
                    view.showMessage(ex.message, -1)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val ex = ru.frosteye.ovsa.execution.network.base.ApiException(
                    getString(R.string.default_connection_error), 0
                )
                if (enableControls)
                    view.enableControls(true, -1)
                view.showMessage(ex.message, -1)
            }

        })
}

